# helloworld-android-buildozer-kivy

El típico HelloWorld funcionando en Android gracias a Buildozer y Kivy

## [opcional] Preparar entorno

```
sudo apt update
sudo apt install build-essential
```
Insertar Guest Additions e ir al directorio donde está montado (algo como /media/usuario/VBox_GAs_6.0.12) y luego `sudo ./VBoxLinuxAdditions.run`
restart

```
mkdir -p proyectos/pycharm
```

## Compartir directorio del proyecto con samba

```
sudo apt install samba
sudo nano /etc/samba/smb.conf
```

agregar recurso compartido por ejemplo:

```
[proyectospycharm]
   comment = Directorio proyectos PyCharm VM
   path = /home/usuario/proyectos/pycharm
   browsable = yes
   read only = no
```

```
sudo service smbd restart
sudo ufw allow samba
sudo smbpasswd -a usuario
```

## Instalar pycharm

```
wget http://download.jetbrains.com/python/pycharm-professional-2019.3.2.tar.gz
mv pycharm-community-2019.3.2.tar.gz /tmp
cd /tmp
tar xzvf pycharm-community-2019.3.2.tar.gz
sudo su -c "chown -R root:root /tmp/pycharm*"
sudo mv /tmp/pycharm-community-2019.3.2 /opt/pycharm-community
sudo ln -s /opt/pycharm-community/bin/pycharm.sh /usr/local/bin/pycharm
sudo ln -s /opt/pycharm-community/bin/inspect.sh /usr/local/bin/inspect
```

ejecutar pycharm desde la terminal
ir a Configure->Create Desktop Entry entonces habrá un ícono en el menú Desarrollo (development)

## Instalar dependencias

```
sudo apt install python3 python3-pip python3-distutils git zip unzip openjdk-8-jdk autoconf libtool pkg-config zlib1g-dev libncurses5-dev libtinfo5 cmake libffi-dev
```

## Crear proyecto
copiar archivos: requirements.txt y main.py

```
buildozer init
buildozer -v android debug
```